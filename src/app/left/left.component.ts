import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Utilities } from '../Services/utilities';

@Component({
  selector: 'app-left',
  templateUrl: './left.component.html',
  styleUrls: ['./left.component.css'],
})
export class LeftComponent implements OnInit {
  limit = new FormControl(10); // Casella input con valore di default

  limitUpdated!: number; // variabile per avere il numero aggiornato

  pokemonTotalNumber!: number; // totale dei pokemon

  totalPages!: number; // numero totale delle pagine
  currentPage!: number ; // pagina attuale

  offset: number = 10;

  pagesSelect=1 // imposto il default per il mio selettore
  


  constructor(public pokeService: Utilities) {}

  ngOnInit(): void {
   
      this.currentPage = 1
      this.totalPages = 112
      
      


    this.limit.valueChanges.subscribe((res: number) => {
      // mi sottoscrivo al valore che cambia
      this.limitUpdated = res;
      this.pokeService
        .httpGetPokemonByPagination(this.offset, this.limitUpdated)
        .subscribe((res) => {
          this.pokeService.getPokemonsData(res.results, res.count);
        });
      this.totalPages = Math.trunc(
        this.pokeService.pokemonsCountTotal / this.limitUpdated
      ); // calcolo il totale della pagine dividendo il numero totale dei pokemon per gli elementi da visualizzare
      if(this.currentPage <= 0){
        console.log("sono in if")
        this.currentPage = 1
      }
      else{
        this.currentPage = Math.trunc((this.offset/this.limit.value)); // calcolo la pagina attuale dividendo il totale delle pagine per gli elementi visualizzati SBAGLIATO
        console.log("sono in else");
      }
    });
  }

  doppiaFrecciaSinistra() {
    this.currentPage = 1
    this.offset = 0;

    this.pagesSelect = this.currentPage // aggiorno la pagina nella select box

    this.pokeService
      .httpGetPokemonByPagination(this.offset, this.limit.value)
      .subscribe((res) => {
        this.pokeService.getPokemonsData(res.results, res.count);
      });
  }

  doppiaFrecciaDestra() {
    this.currentPage = this.totalPages = Math.trunc(
      this.pokeService.pokemonsCountTotal / this.limit.value
    );

    this.pagesSelect = this.currentPage // aggiorno la pagina nella select box
    
    this.offset = this.pokeService.pokemonsCountTotal - (this.pokeService.pokemonsCountTotal % this.limit.value);
    this.pokeService
      .httpGetPokemonByPagination(this.offset, this.limit.value)
      .subscribe((res) => {
        this.pokeService.getPokemonsData(res.results, res.count);
      });
  }
  frecciaSinistra() {

    if(this.currentPage != 1){
      this.currentPage = this.currentPage - 1
      this.offset = this.offset - this.limit.value;
  
      this.pagesSelect = this.currentPage // aggiorno la pagina nella select box
  
      this.pokeService
        .httpGetPokemonByPagination(this.offset, this.limit.value)
        .subscribe((res) => {
          this.pokeService.getPokemonsData(res.results, res.count);
        });
    }
  }
  frecciaDestra() {
 
    if(this.currentPage <= this.totalPages - 1){
      console.log("freccia destra IF")
      this.offset = this.currentPage * this.limit.value;
      this.currentPage = this.currentPage + 1
      
  
      this.pagesSelect = this.currentPage // aggiorno la pagina nella select box
  
      this.pokeService
        .httpGetPokemonByPagination(this.offset, this.limit.value)
        .subscribe((res) => {
          this.pokeService.getPokemonsData(res.results, res.count);
        });
        console.log(this.offset,this.limit.value)
    }
    else{
      console.log("attaccati al tram")
    }
       
    
  

  }

  counter(i: number) {
    // console.log (new Array(Number))
    return new Array(i);    
}

  pagesChange(pagina:any){
    this.currentPage = pagina 

    this.offset = this.currentPage * this.limit.value - this.limit.value


    this.pokeService
    .httpGetPokemonByPagination(this.offset, this.limit.value)
    .subscribe((res) => {
      this.pokeService.getPokemonsData(res.results, res.count);
    });

    console.log(this.pagesSelect) 
    
    

  }

  clickOnPokeball(url:string){
    if(this.pokeService.pokeDetails.length <=1){
      this.currentPage = 1;
      this.pokeService.httpGetPokemonDetail(url).subscribe((res)=>{this.pokeService.getPokemonDetail (res)})
    }
    else{
      // ($('#exampleModal') as any).modal('show');
      window.alert(`Si possono visualizzare al massimo due pokemon per volta. 
Elimina almeno un pokemon per visualizarne di nuovi`)
    }
  }

}
